import Body from "./Body/Body";
import BodySlide from "./Body/Slide";
import Footer from "./Footer/Footer";
import Header from "./Header/Header";

const Course365UI = () =>{
    return (
        <>
            <Header/>
            <BodySlide/>
            <Body/>
            <Footer/>
        </>
    )
}
export default Course365UI;