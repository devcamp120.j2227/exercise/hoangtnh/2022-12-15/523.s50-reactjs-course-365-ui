import { Grid,InputAdornment, OutlinedInput, IconButton } from "@mui/material";
import logo from "../../assets/images/Ionic_logo.png"
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import StyleButton from "../styledCss/ButtonHeader";
import SearchIcon from '@mui/icons-material/Search';
const Header = () =>{
    return(
        <div className="header-page" >
            <Grid container spacing={2} alignItems="center">
                <Grid item xs={2} textAlign="center">
                    <img src={logo} alt="logo"/>
                </Grid>
                <Grid container xs={7} textAlign="left" alignItems="center">
                    <StyleButton><p><b>Home</b></p></StyleButton>
                    <StyleButton><p>Browse course</p><ExpandMoreIcon/></StyleButton>
                    <StyleButton><p>About us </p><ExpandMoreIcon/></StyleButton>
                </Grid>
                <Grid container xs={3} alignItems="center">
                    <Grid variant="outlined" className="search-course">
                        <OutlinedInput  style={{height:"40px"}}
                            startAdornment={
                            <InputAdornment position="start">
                                <IconButton >
                                    <SearchIcon/>
                                </IconButton>
                            </InputAdornment>
                            }
                            placeholder="Search course"
                        />
                    </Grid>
                    <Grid >
                        <StyleButton type="primary">Search</StyleButton>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}
export default Header;