import { Grid } from "@mui/material";
import Link from '@mui/material/Link';
import { Container } from "@mui/system";
const Footer = () =>{
    return(
        <div className="footer-page" >
            <Container>
                <Grid container spacing={2} >
                    <Grid item xs={6} textAlign="left" alignItems="center" style={{paddingTop:"10px"}}>
                        <p>© 2021 Ionic Course365. All Rights Reserved.</p>
                    </Grid>
                    <Grid container xs={6} justifyContent="flex-end" alignItems="center" style={{paddingTop:"10px"}}>
                        <Link item xs={3} m={2} href="#" underline="hover">
                            {'Privacy'}
                        </Link>
                        <Link item xs={3} m={2} href="#" underline="hover">
                            {'Terms'}
                        </Link>
                        <Link item xs={3} m={2} href="#" underline="hover">
                            {'Feedback'}
                        </Link>
                        <Link item xs={3} m={2} href="#" underline="hover">
                            {'Support'}
                        </Link>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}
export default Footer;