import styled from "styled-components";
const StyleButton = styled.button`
${props => props.type && props.type === "primary" ? 
        `color: #17a2b8; border: 1px solid #17a2b8; border-radius:5px;background-color: #f8f9fa;`: 
        `background-color: #f8f9fa; color: black; border: none;
    `}
    font-size: 0.9rem;
    padding: 10px;
    display: flex;
    align-items: center;
    margin-top: 10px
`
export default StyleButton;